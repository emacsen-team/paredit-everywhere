(define-package "paredit-everywhere" "0.4git10" "Enable some paredit features in non-lisp buffers"
  '((paredit "22"))
  :url "https://github.com/purcell/paredit-everywhere.git")
